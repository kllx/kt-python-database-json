from models import *
from db import *

from flask import Flask, render_template, request, redirect


#Создаем экземпляр приложения
app = Flask(__name__)

Database.load()

@app.route('/')
def index_route():
    return render_template('index.html', markets=Database.get_markets(), persons=Database.get_persons())

@app.route('/market/<int:id>')
def market_route(id):
    return render_template('market.html', market=Database.get_market(id))

@app.route('/market/add', methods=['GET', 'POST'])
def market_add():
    if request.method == 'GET':
        return render_template('add_market.html')
    elif request.method == 'POST':
        market = Market(request.form['market_name'], request.form['market_balance'])
        Database.add_market(market)
        Database.save()
        return redirect('/')

@app.route('/market/<int:id>/edit', methods=['GET', 'POST'])
def market_edit(id):
    market = Database.get_market(id)
    if request.method == 'GET':
        return render_template('edit_market.html', market=market)
    elif request.method == 'POST':
        market.name = request.form['market_name']
        Database.save()
        return redirect('/market/{}'.format(id))

@app.route('/market/<int:id>/addcar', methods=['GET', 'POST'])
def market_addcar(id):
    market = Database.get_market(id)
    if request.method == 'GET':
        return render_template('add_car_to_market.html', market=market)
    elif request.method == 'POST':
        car = Database.get_car(int(request.form['car_id']))
        car.add_history(market)
        market.add_car(car)
        Database.save()
        return redirect('/market/{}'.format(id))


@app.route('/car/add', methods=['GET', 'POST'])
def car_add():
    if request.method == 'GET':
        return render_template('add_car.html')
    elif request.method == 'POST':
        car = Car(request.form['car_name'], request.form['car_price'])
        Database.add_car(car)
        Database.save()
        return redirect('/')

@app.route('/car/<int:id>')
def car_route(id):
    return render_template('car.html', car=Database.get_car(id))


@app.route('/person/add', methods=['GET', 'POST'])
def person_add():
    if request.method == 'GET':
        return render_template('add_person.html')
    elif request.method == 'POST':
        person = Person(request.form['person_name'], request.form['person_balance'])
        Database.add_person(person)
        Database.save()
        return redirect('/')

@app.route('/person/<int:id>')
def person(id):
    return render_template('person.html', person=Database.get_person(id))

@app.route('/person/<int:id>/addcar', methods=['GET', 'POST'])
def person_addCar(id):
    person = Database.get_person(id)
    if request.method == 'GET':
        return render_template('add_car_to_person.html', person=person)
    elif request.method == 'POST':
        car = Database.get_car(int(request.form['car_id']))
        car.add_history(person)
        person.add_car(car)
        Database.save()
        return redirect('/person/{}'.format(id))


if __name__ == '__main__':
    app.run()