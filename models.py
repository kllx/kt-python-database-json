from db import *
import json

class Person:
    def __init__(self, fullname, balance, id = None):
        self.id = id if id is not None else Database.new_id()
        self.fullname = fullname
        self.balance = balance
        self.cars = []

    def add_money(self, sum):
        self.balance += sum

    def rem_money(self, sum):
        self.balance -= sum

    def add_car(self, car):
        self.cars.append(car)

    def rem_car(self, car):
        self.cars.remove(car)

    def to_json(self):
        result = {}
        result['id'] = self.id
        result['fullname'] = self.fullname
        result['balance'] = self.balance
        result['cars'] = [car.id for car in self.cars]
        return result

class Car:
    def __init__(self, name, price, id = None):
        self.id = id if id is not None else Database.new_id()
        self.name = name
        self.price = price
        self.history = []

    def add_history(self, person):
        self.history.append(person)

    def to_json(self):
        result = {}
        result['id'] = self.id
        result['name'] = self.name
        result['price'] = self.price
        result['history'] = [person.id for person in self.history]
        return result

class Market:
    def __init__(self, name, balance, id = None):
        self.id = id if id is not None else Database.new_id()
        self.name = name
        self.balance = balance
        self.cars = []

    def add_money(self, sum):
        self.balance += sum

    def rem_money(self, sum):
        self.balance -= sum

    def add_car(self, car):
        self.cars.append(car)

    def rem_car(self, car):
        self.cars.remove(car)

    def to_json(self):
        result = {}
        result['id'] = self.id
        result['name'] = self.name
        result['balance'] = self.balance
        result['cars'] = [car.id for car in self.cars]
        return result