import json

class Database:
    current_id = 0

    persons = {}
    cars = {}
    markets = {}

    @staticmethod
    def new_id():
        Database.current_id += 1
        return Database.current_id

    @staticmethod
    def add_person(person):
        Database.persons[person.id] = person

    @staticmethod
    def rem_person(person):
        Database.persons[person.id] = None

    @staticmethod
    def get_person(id):
        return Database.persons[id]

    @staticmethod
    def get_persons():
        return Database.persons.values()

    @staticmethod
    def add_car(car):
        Database.cars[car.id] = car

    @staticmethod
    def rem_car(car):
        Database.cars[car.id] = None

    @staticmethod
    def get_car(id):
        return Database.cars[id]

    @staticmethod
    def get_cars():
        return Database.cars.values()

    @staticmethod
    def add_market(market):
        Database.markets[market.id] = market

    @staticmethod
    def rem_market(market):
        Database.markets[market.id] = None

    @staticmethod
    def get_market(id):
        return Database.markets[id]

    @staticmethod
    def get_markets():
        return Database.markets.values()

    @staticmethod
    def save():
        result = {}
        result['system'] = {}
        result['system']['current_id'] = Database.current_id

        result['collections'] = {}
        result['collections']['persons'] \
            = {person.id:person.to_json() for _, person in Database.persons.items()}

        result['collections']['cars'] \
            = {car.id:car.to_json() for _, car in Database.cars.items()}

        result['collections']['markets'] \
            = {market.id:market.to_json() for _, market in Database.markets.items()}

        with open('db.json', mode='w') as file:
            file.write(json.dumps(result))

        return True

    @staticmethod
    def load():
        from models import Market, Car, Person

        data = {}

        with open('db.json', mode='r') as file:
            data = json.loads(file.read())

        Database.current_id = int(data['system']['current_id'])

        data_market = data['collections']['markets']
        for value in data_market.values():
            market = Market(value['name'], value['balance'], id=value['id'])
            Database.add_market(market)

        data_cars = data['collections']['cars']
        for value in data_cars.values():
            car = Car(value['name'], value['price'], id=value['id'])
            Database.add_car(car)

        data_persons = data['collections']['persons']
        for value in data_persons.values():
            person = Person(value['fullname'], value['balance'], id=value['id'])
            Database.add_person(person)

