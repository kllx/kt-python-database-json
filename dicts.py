class Sample:
    def __init__(self, id, name):
        self.id = id
        self.name = name

    def __repr__(self):
        return 'Sample [id:{}] [name:{}]'.format(self.id, self.name)

samples = {}

samples['first'] = Sample(1, 'First Sample')
samples['second'] = Sample(2, 'Second Sample')
samples['third'] = Sample(3, 'Third Sample')

#Список ключей
#print(list(samples.keys()))

#Список значений
#print(list(samples.values()))

#Список item
#print(list(samples.items()))

#Список имен сэмплов
#names_list = [sample.name for sample in samples.values()]
#print(names_list)

#names_list = []
#for sample in samples.values():
#    names_list.append(sample.name)
#print(names_list)

#objects_dict = {}
#for sample in samples.values():
#    objects_dict[sample.id] = sample.name
#print(objects_dict)

objects_dict = {sample.id:sample.name for sample in samples.values()}
print(objects_dict)